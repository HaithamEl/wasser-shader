# Shader World El Euch

**Willkommen im Shader World Repository.**

Im Rahmen der Projektphase entwickle ich ein paar unterschiedliche Shader und binde sie in ein kleines Spiel. Das Spiel besteht aus einer Insel, auf der verschiedene Objekte wie auch die Shader zu finden sind, und einem First-Person-Controller, mit dem man sich auf der Insel bewegen und die Shader betrachten kann.

### Download
- [Game [Windows]](https://haitham-el-euch.itch.io/shader-world)
- [Game [MacOS]](https://haitham-el-euch.itch.io/shader-world)
- [Source Code](https://gitlab.com/HaithamEl/wasser-shader/-/releases/v1.1)

Unity wird dabei als Rendering Engine verwendet. Die Programmierung erfolgt mittels Shader Lab und CG/HLSL sowie dem Shader Graph. 

**Benutzt wird:**
- Unity Version 2021.3.10f1 (LTS)
- Universal Render Pipeline (URP)

In der Projektphase geht es darum, sich mit dem Thema (Shader) mehr auseinander zusetzen und sich ein Überblick zu verschaffen. Sodass man im Anschluss darauf, eine geeignete Thesis findet, um dann mit dem Verfassen der Bachelorarbeit anzufangen.
