Shader "ShaderWorld/WaterShader"
{
    Properties
    {
        [Header(Surface Options)]
        [Space(10)]
        _DepthColorLight("Water Depth Color Light", Color) = (0.32, 0.805, 0.970, 0.70)
        _DepthColorDark("Water Depth Color Dark", Color) = (0.085, 0.405, 1, 0.75)
        _DepthMaxDistance("Depth Maximum Distance", Float) = 1
        
        [Header(Shoreline Foam Options)]
        [Space(10)]
        _Noise("Foam Noise", 2D) = "white" {}
        _NoiseCutoff("Foam Noise Cutoff", Range(0, 1)) = 0.7
        _NoiseScroll("Foam Noise Scroll Amount", Vector) = (0.025, 0.025, 0, 0)
        
        _ShorelineFoamDistortion("Distortion Texture", 2D) = "white" {}
        _DistortionAmount("Distortion Amount", Range(0, 5)) = 5
        
        _ShorelineFoamDistance("Foam Distance", Float) = 0.5
        _ShorelineFoamColor("Foam Color", Color) = (1,1,1,1)
        
        [Header(Wave Options)]
        [Space(10)]
        _WaveSpeed("Wave Speed", Range(0, 10)) = 1
        _WavePeriod("Wave Period", Range(0, 20)) = 12
        _WaveAmp("Wave Amplitude", Range(0, 1)) = 0.125
        
        [Header(Normal Map Options)]
        [Space(10)]
        _NormalTex ("Texture", 2D) = "bump" {}
        _NormalTex2 ("Texture", 2D) = "bump" {}
        _NormalIntensity ("Normal Inensity", Range(0,1)) = 1
        
        [Header(Lighting and Reflection Options)]
        [Space(10)]
        _DiffuseIBL ("Diffuse IBL", 2D) = "black" {}
        _SpecularIBL ("Specular IBL", 2D) = "black" {}
        _Color ("Color", Color) = (1, 1, 1, 1)
        _Gloss ("Gloss", Range(0, 1)) = 1
        _Smoothness("Smoothness", Float) = 0
        _SpecIBLIntensity ("Specular IBL Intensity", Range(0,1)) = 1
        [Toggle] _isFresnel("Fresnel On/Off", Float) = 0
        _FresnelIntensity ("Fresnel Intensity", Range(0, 50)) = 10
    }
    SubShader
    {
        Tags 
        { 
            "RenderPipeline" = "UniversalPipeline" 
            "Queue" = "Transparent" 
            "RenderType" = "Transparent" 
            "LightMode" = "UniversalForward"
        }

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
            Cull Off
            
            HLSLPROGRAM

            #define SPECULAR_COLOR

            #pragma vertex Vertex
            #pragma fragment Fragment

            #include "WaterPass.hlsl"
            
            ENDHLSL
        }
    }
}
