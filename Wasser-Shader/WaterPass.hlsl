#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

SAMPLER(_CameraDepthTexture);

TEXTURE2D(_Noise); SAMPLER(sampler_Noise);
TEXTURE2D(_ShorelineFoamDistortion); SAMPLER(sampler_ShorelineFoamDistortion);

TEXTURE2D(_NormalTex); SAMPLER(sampler_NormalTex);
TEXTURE2D(_NormalTex2); SAMPLER(sampler_NormalTex2);

TEXTURE2D(_DiffuseIBL); SAMPLER(sampler_DiffuseIBL);
TEXTURE2D(_SpecularIBL); SAMPLER(sampler_SpecularIBL);

// Matching property variables
CBUFFER_START(UnityPerMaterial)
float4 _DepthColorLight;
float4 _DepthColorDark;
float _DepthMaxDistance;

float4 _Noise_ST;
float _NoiseCutoff;
float2 _NoiseScroll;

float4 _ShorelineFoamDistortion_ST;
float _DistortionAmount;

float _ShorelineFoamDistance;
float4 _ShorelineFoamColor;

float _WaveSpeed;
float _WavePeriod;
float _WaveAmp;

float4 _NormalTex_ST;
float4 _NormalTex2_ST;

float _Gloss;
float _Smoothness;
float4 _Color;
float _NormalIntensity;
float _SpecIBLIntensity;
float _isFresnel;
float _FresnelIntensity;
CBUFFER_END

#define SMOOTHSTEP_ANTIALIASING 0.01
#define TAU 6.28318530718

float4 alphaBlending(float4 top, float4 bottom)
{
    float3 color = (top.rgb * top.a) + (bottom.rgb * (1 - top.a));
    float alpha = top.a + bottom.a * (1 - top.a);

    return float4(color, alpha);
}

float2 DirToRectilinear( float3 direction )
{
    float x = atan2( direction.z, direction.x ) / TAU + 0.5;
    float y = direction.y * 0.5 + 0.5;
    return float2(x,y);
}

struct MeshData
{
    float3 positionOS : POSITION;
    float2 uv : TEXCOORD0;
    float3 normalOS : NORMAL;
    float4 tangentOS : TANGENT;
    
};

struct Interpolators
{
    float4 positionCS : SV_POSITION;
    float4 screenPosition : TEXCOORD1;
    float2 noiseUV : TEXCOORD0;
    float2 distortUV : TEXCOORD2;
    float3 positionWS : TEXCOORD3;
    float3 normalWS : TEXCOORD4;
    float2 uv_normal : TEXCOORD5;
    float2 uv_normal2 : TEXCOORD6;
    float3 tangentWS : TEXCOORD7;
    float3 binormalWS : TEXCOORD8;
};

Interpolators Vertex(MeshData input)
{
    Interpolators output;
    
    input.positionOS.y += cos(input.uv.x * _WavePeriod + _Time.y * _WaveSpeed) * _WaveAmp;

    VertexPositionInputs pos_inputs = GetVertexPositionInputs(input.positionOS);

    output.positionCS = pos_inputs.positionCS;
    output.screenPosition = ComputeScreenPos(output.positionCS);
    output.noiseUV = TRANSFORM_TEX(input.uv, _Noise);
    output.distortUV = TRANSFORM_TEX(input.uv, _ShorelineFoamDistortion);

    output.uv_normal = TRANSFORM_TEX(input.uv, _NormalTex);
    output.uv_normal2 = TRANSFORM_TEX(input.uv, _NormalTex2);

    output.positionWS = pos_inputs.positionWS;
    output.normalWS = normalize(TransformObjectToWorldNormal(input.normalOS));
    output.tangentWS = normalize(mul(unity_ObjectToWorld, input.tangentOS).xyz);
    output.binormalWS = cross(output.normalWS, output.tangentWS);
    output.binormalWS *= (input.tangentOS.w * unity_WorldTransformParams.w);

    return output;
}

float4 Fragment(Interpolators input) : SV_TARGET
{
    // Depth based Color
    float depthNonLinear = tex2Dproj(_CameraDepthTexture, input.screenPosition).r;
    float depthLinear = LinearEyeDepth(depthNonLinear, _ZBufferParams);
    float waterDepth = depthLinear - input.screenPosition.w;

    float depthDifference = saturate(waterDepth / _DepthMaxDistance);
    float4 waterColor = lerp(_DepthColorLight, _DepthColorDark, depthDifference);

    // Shoreline Foam
    float2 distortion = (SAMPLE_TEXTURE2D(_ShorelineFoamDistortion, sampler_ShorelineFoamDistortion, input.distortUV).xy * 2 - 1) * _DistortionAmount;
    float2 noiseUV = float2((input.noiseUV.x + _Time.y * _NoiseScroll.x) + distortion.x, (input.noiseUV.y + _Time.y * _NoiseScroll.y) + distortion.y);
    float noise = SAMPLE_TEXTURE2D(_Noise, sampler_Noise, noiseUV).r;
    
    float shorelineFoamDepthDifference = saturate(depthDifference / _ShorelineFoamDistance);
    float NoiseCutoff = shorelineFoamDepthDifference * _NoiseCutoff;
    float shorelineNoise = smoothstep(NoiseCutoff - SMOOTHSTEP_ANTIALIASING, NoiseCutoff + SMOOTHSTEP_ANTIALIASING, noise);
    float4 NoiseColor = _ShorelineFoamColor;
    NoiseColor.a *= shorelineNoise;

    // Normal Map
    float2 noiseUV1 = float2(input.uv_normal.x + _Time.y * 0.03, input.uv_normal.y + _Time.y * 0.03);
    float2 noiseUV2 = float2(input.uv_normal2.x + _Time.y * -0.05, input.uv_normal2.y + _Time.y * 0.01);
    float4 normal_map = SAMPLE_TEXTURE2D(_NormalTex, sampler_NormalTex, noiseUV1);
    float4 normal_map2 = SAMPLE_TEXTURE2D(_NormalTex2, sampler_NormalTex2, noiseUV2);
    float3 normal_compressed = UnpackNormal(normal_map);
    float3 normal_compressed2 = UnpackNormal(normal_map2);
    normal_compressed = normalize(lerp(float3(0,0,1), normal_compressed, _NormalIntensity));
    normal_compressed2 = normalize(lerp(float3(0,0,1), normal_compressed2, _NormalIntensity));

    float3x3 TBN_matrix = float3x3
    (
     input.tangentWS.x, input.binormalWS.x, input.normalWS.x,
     input.tangentWS.y, input.binormalWS.y, input.normalWS.y,
     input.tangentWS.z, input.binormalWS.z, input.normalWS.z
    );

    float3 normal_color = mul(TBN_matrix, normal_compressed);
    float3 normal_color2 = mul(TBN_matrix, normal_compressed2);

    // Lighting
    InputData lightingInput = (InputData)0;
    lightingInput.positionWS = input.positionWS;
    lightingInput.normalWS = normalize(input.normalWS);
    lightingInput.viewDirectionWS = GetWorldSpaceNormalizeViewDir(input.positionWS);
    
    SurfaceData surfaceInput = (SurfaceData)0;
    surfaceInput.albedo = _Color.rgb;
    surfaceInput.alpha = _Color.a;
    surfaceInput.specular = _Smoothness;
    surfaceInput.smoothness = _Smoothness;
    
    // Diffuse lighting
    float3 Nws = normalize(input.normalWS);
    float3 N = normalize(normal_color + normal_color2);
    float4 shadowMask = CalculateShadowMask(lightingInput);
    AmbientOcclusionFactor aoFactor = CreateAmbientOcclusionFactor(lightingInput, surfaceInput);
    Light L = GetMainLight(lightingInput, shadowMask, aoFactor);
    float3 lambert = saturate(dot(N, L.direction));
    float3 diffuseLight = lambert * L.color.rgb;

    // Diffuse IBL
    float3 diffuseIBL = SAMPLE_TEXTURE2D_LOD(_DiffuseIBL, sampler_DiffuseIBL, DirToRectilinear(N), 0).xyz;
    diffuseLight += diffuseIBL;

    // Specular lighting
    float3 V = lightingInput.viewDirectionWS;
    float3 H = normalize(L.direction + V);
    float3 specularLight = saturate(dot(H, N)) * (lambert > 0);
    float specularExponent = exp2(_Gloss * 11) + 2;
    specularLight = pow(specularLight, specularExponent) * _Gloss;
    specularLight *= L.color.rgb;

    // Specular IBL
    float3 view_refl = reflect(-V, N);
    float mipLVL = (1 - _Gloss) * 6;
    float3 specularIBL = SAMPLE_TEXTURE2D_LOD(_SpecularIBL, sampler_SpecularIBL, DirToRectilinear(view_refl), mipLVL).xyz;
    specularLight += specularIBL * _SpecIBLIntensity;

    // Fresnel
    float fresnel = saturate(1-dot(V, Nws));
    fresnel = pow(fresnel, _FresnelIntensity);
    
    return alphaBlending(NoiseColor, waterColor) + float4(diffuseLight * _Color.rgb + specularLight + _isFresnel * fresnel, 0);
}